import setuptools

with open("readme.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name='SpaceSimulator',
    version='0.2.0',
    author="Fabio D'Elia, Mattia Sgro, Michele Pugno",
    description='Space-themed shooter arcade game, heavily inspired by Asteroids.',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/MichelePugno/pygameprojectlp/',
    project_urls={
        "Bug Tracker": "https://gitlab.com/MichelePugno/pygameprojectlp/issues",
    },
    packages=['src/SpaceSimulator'],
    install_requires=['pygame == 2.1.2'],
    # python_requires='>=3.9',
)
