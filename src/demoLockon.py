"""This demo shows the lockon camera feature.
The green object does not move, the red object with the text is moving and is followed by the camera."""

from distutils.log import debug
from Game.GameScenario import GameScenario
from SpaceSimulator.Scenario import Scenario
from SpaceSimulator.SimObject import SimObject
import random
import pygame


class Static(SimObject):
    def __init__(self, scenario: Scenario, position, image: pygame.Surface, debug=False, lock_on=False) -> None:
        super().__init__(scenario, position, image, debug, lock_on)

    def __str__(self):
        return "I am not moving"


pygame.init()
instance = GameScenario(screen = pygame.display.set_mode((1366,768)),debug=True)

for x in range(200):
    position = [random.randint(1, instance.map_area[0]),
                random.randint(1, instance.map_area[1])]
    image = pygame.Surface([10, 10], pygame.SRCALPHA, )
    image.fill([0, 0, 255])
    test = SimObject(instance, position, image)

    test.speed = [random.uniform(-100, 100), random.uniform(-100, 100)]
    instance.all.add(test)
image = pygame.Surface([20, 20], pygame.SRCALPHA, )
image.fill([255, 0, 0])
test = SimObject(instance, [300, 300], image,  debug=True, lock_on=True)
test.speed = [50, 50]
instance.all.add(test)
image = pygame.Surface([10, 10], pygame.SRCALPHA, )
image.fill([0, 255, 0])
test = Static(instance, [500, 500], image, debug=True)
instance.all.add(test)
test = Static(instance, [1000, 500], image, debug=True)
instance.all.add(test)
instance.main_loop()
