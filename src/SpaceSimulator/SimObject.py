"""Main simulation component"""

import pygame
import math
from SpaceSimulator import Scenario


class SimObject(pygame.sprite.Sprite):
    """Generic game object. Implements sprites rotation and movement.

        Args:
            scenario (Scenario.Scenario): the simulation scenario.
            position (list): the initial position of the object as [x, y].
            image (pygame.Surface): the surface of the pygame.sprite.
            debug (bool, optional): enable debug mode: display sprite rect and __str__ on screen. Defaults to False.
            lock_on (bool, optional): camera will follow the object. Defaults to False."""

    def __init__(self, scenario: Scenario,
                 position: list,
                 image: pygame.Surface,
                 debug=False, lock_on=False) -> None:
        super().__init__()

        self._scenario = scenario
        self._image = image.convert()
        self._size = self._image.get_size()
        self._origin = position
        self._labelposition = position  # required in debug
        self._lock_on = lock_on
        self._image_update = True  # Will update image
        # initialize image attributes
        self._rotated_image = None
        self._pivot_move = None

        self.position = position
        self.speed = [0.0, 0.0]  # [x, y]
        self.max_speed = math.inf
        self.angle = 0.0
        self.acceleration = 0.0
        self.rect = self._image.get_rect(center=position)
        self.debug = debug

    def __str__(self):
        return """SimObject"""

    def accelerate(self, angle, acceleration):
        """accelerate the object

        Args:
            angle (float): the angle of the acceleration vector according to the display frame
            acceleration (float): the magnitude of the acceleration vector in pixel/s^2
        """
        cos = math.cos(math.radians(angle))
        sin = math.sin(math.radians(angle))

        rel_max_speed = [self.max_speed * cos,
                         self.max_speed * -sin]

        if rel_max_speed[0] > 0:
            if self.speed[0] <= rel_max_speed[0]:
                self.speed[0] += acceleration * \
                    cos * self._scenario.delta_time
        else:
            if self.speed[0] >= rel_max_speed[0]:
                self.speed[0] += acceleration * \
                    cos * self._scenario.delta_time

        if rel_max_speed[1] > 0:
            if self.speed[1] <= rel_max_speed[1]:
                self.speed[1] += acceleration * - \
                    sin * self._scenario.delta_time
        else:
            if self.speed[1] >= rel_max_speed[1]:
                self.speed[1] += acceleration * - \
                    sin * self._scenario.delta_time

    def spin(self, value):
        """spin spin the object by value {float} degree

        Args:
            value (float): angle in degrees
        """

        if value != 0:
            self.angle += value
            self._image_update = True

    def is_in_screen(self) -> bool:
        """is_in_screen

        Checks if an object is visible

        Returns:
            bool: True if object is within _scenario.camera range
        """

        if (abs(self._scenario.camera_x - self.position[0]) < (self._scenario.screen_size[0] / 2) + self.rect.width / 2 and
                abs(self._scenario.camera_y - self.position[1]) < (self._scenario.screen_size[1] / 2) + self.rect.height / 2):
            return True
        return False

    def image_handler(self):
        """image_handler rotates self._image according to self.angle."""

        box = [pygame.math.Vector2(p) for p in [(0, 0), (self._size[0], 0),
               (self._size[0], -self._size[1]), (0, -self._size[1])]]
        box_rotate = [p.rotate(self.angle) for p in box]
        self.min_box = (min(box_rotate, key=lambda p: p[0])[0],
                        min(box_rotate, key=lambda p: p[1])[1])
        self.max_box = (max(box_rotate, key=lambda p: p[0])[0],
                        max(box_rotate, key=lambda p: p[1])[1])

        # calculate the translation of the pivot
        pivot = pygame.math.Vector2(self._size[0] / 2, -self._size[1] / 2)
        pivot_rotate = pivot.rotate(self.angle)
        self._pivot_move = pivot_rotate - pivot

        self._rotated_image = pygame.transform.rotate(self._image, self.angle)

    def display_rect(self):
        """display_rect only in debug mode.

        Display sprite rect on screen."""

        new_rect = self.rect.copy()
        new_rect.topleft = self._debug_position
        pygame.draw.rect(self._scenario.screen, (255, 0, 0),
                         new_rect, 1)

    def display_label(self):
        """display_label 

        In debug mode displays object __str__ as a label."""

        values = self.__str__()
        values = values.split("\n")
        offset = 0

        for string in values:
            offset += 30
            display_label = self._scenario.debug_font.render(
                string, 1, (255, 255, 0))
            self._scenario.screen.blit(display_label, (self._debug_position[0],
                                                       self._debug_position[1] + offset))

    def blit_on_screen(self):
        """blit_on_screen method

        Blits on pygame.display the object according to its attributes."""
        # lockon
        if self._lock_on:
            # user ship handling with camera position
            self._scenario.follow_object(self)
            w, h = self._rotated_image.get_size()
            x = (self._scenario.screen_size[0] / 2.0) - w / 2
            y = (self._scenario.screen_size[1] / 2.0) - h / 2
            self._labelposition = ((self._scenario.screen_size[0] / 2.0) - self.rect.width / 2,
                                   (self._scenario.screen_size[1] / 2.0) - self.rect.height / 2)
            # blitting
            self._scenario.screen.blit(self._rotated_image, (x, y))

        # normal camera
        elif self.is_in_screen():
            # calculate the center origin of the rotated image
            self._origin = (self.position[0] - self._size[0] / 2 + self.min_box[0] - self._pivot_move[0],
                            self.position[1] - self._size[1] / 2 - self.max_box[1] + self._pivot_move[1])
            # blitting
            self._scenario.screen.blit(self._rotated_image,
                                       ((self._scenario.screen_size[0] / 2) - ((self._scenario.camera_x - self._origin[0])),
                                        (self._scenario.screen_size[1] / 2) - ((self._scenario.camera_y - self._origin[1]))))

        if self.is_in_screen() and self.debug:
            # calculate the debug info position
            self._debug_position = ((self._scenario.screen_size[0] / 2) - (self._scenario.camera_x - self.rect.x),
                                    (self._scenario.screen_size[1] / 2) - (self._scenario.camera_y - self.rect.y))
            self.display_label()
            self.display_rect()

    def kill(self):
        """Placeholder for future kill functions, called by collision function calls."""
        super().kill()

    def update(self, *args):
        """update, overridden sprite.update method.

        Updates sprite position and image."""

        # redefine position and rotate sprite image
        self.position[0] += self.speed[0] * self._scenario.delta_time
        self.position[1] += self.speed[1] * self._scenario.delta_time
        self.rect.centerx = int(self.position[0])
        self.rect.centery = int(self.position[1])

        # update image
        if self._image_update and self.is_in_screen():
            self.image_handler()
            self._image_update = False

        self.blit_on_screen()
