"""Simulation Scenario
It represents the simulation session and the pygame screen instance.
"""

import pygame


class Scenario():
    """Scenario _summary_

        Args:
            screen (pygame.Surface, optional): pass the target display object, otherwise a new one will be created. Defaults to None.
            screen_size (tuple, optional): pygame.display screen size. This will be rescaled. Defaults to (1920, 1080).
            bgd_color (list, optional): pygame.display background RGB filling. Defaults to [0, 0, 0].
            debug (bool, optional): enable debug: more informations will be displayed at runtime. Defaults to False.
            testing (bool, optional): create headless display, only for testing purposes. Defaults to False.
"""

    def __init__(self,
                 screen: pygame.Surface,
                 screen_size,
                 bgd_color=[0, 0, 0],
                 debug=False, testing=False,
                 done=False) -> None:

        self.debug_font = pygame.font.SysFont("monospace", 15)
        self.debug = debug
        self.done = done  # Terminates the simulation if True
        self.bgd_color = bgd_color

        # Setting up the screen
        self.screen = screen
        self.screen.fill(self.bgd_color)
        self.screen_size = screen_size
        self.screen_size = self.screen.get_size()

        self.camera_x = self.screen_size[0]/2
        self.camera_y = self.screen_size[1]/2

        # Simulation area
        self.map_area = self.screen_size

        # Game clock setting
        self.clock = pygame.time.Clock()
        self.delta_time = 0.0

        # Sprite groups
        self.particles = pygame.sprite.Group()
        self.all = pygame.sprite.Group()

    def purge(self):
        """purge removes all the sprites from the scenario"""
        self.all.clear(self.screen, self.screen)
        self.all.empty()

    def follow_object(self, obj):
        """Follow an object in the screen"""
        self.camera_x = int(obj.position[0])
        self.camera_y = int(obj.position[1])

    def main_logic(self):
        """The main logic of this scenario"""
        # keys events
        keys = pygame.key.get_pressed()
        self.particles.update()
        self.all.update(keys)

    def freeze(self):
        """Freeze the simulation"""
        self.done = True

    def unfreeze(self):
        """Unfreeze the simulation"""
        self.done = False


    def main_loop(self):
        """Main simulation loop"""
        self.delta_time = self.clock.tick_busy_loop(60) / 1000

        # check for exit
        while not self.done:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    exit()
            self.clock.tick_busy_loop(60) # 60 fps Fix for pausing game

            # Refresh screen and execute logic
            self.screen.fill(self.bgd_color)
            self.main_logic()

            if self.debug:
                display_label = self.debug_font.render(" Sprites in game:" +
                                                       str(len(self.all.sprites())) +
                                                       " Particles in game:" +
                                                       str(len(self.particles.sprites())) +
                                                       ", fps: " +
                                                       str(self.clock.get_fps()) +
                                                       ", position: x=" +
                                                       str(self.camera_x) +
                                                       ", y=" +
                                                       str(self.camera_y),
                                                       1, (255, 255, 0))

                self.screen.blit(display_label, (0, 0))
            pygame.display.update()
