"""Particle generator objects"""

import random
import math
from SpaceSimulator import Scenario, Particle

class StandardGenerator():
    """StandardGenerator

    Args:
        scenario (Scenario.Scenario): belonging to scenario.
        position (_type_): position of the generator.
        colors (list of tuples): possible colors of the particle as a list of 3 elements tuples.
        size (list, optional): dimension of the particles. Defaults to [1, 1].
        t2l (float, optional): Maximum lifespan of the particles. Default to 1.0.
    """

    def __init__(self, scenario: Scenario, position: list, colors: list, size=[1, 1], t2l=1.0) -> None:
        self.scenario = scenario
        self.position = position
        self.colors = colors
        self.size = size
        self.num_colors = len(colors)-1
        self.t2l = t2l

    def generate(self, part_speed=30.0, density=2.0):
        """generate

        Particles are generated by the object in a non specified fashion.
        Origin of the particles is the position of the generator.

        Args:
            part_speed (float, optional): Maximum speed of the particles. Defaults to 30.0.
            density (float, optional): Density factor of the generation. Number of generated particles is equal to the ceil of rand[0, 1) * density.  Defaults to 2.0.
        """

        amount = math.ceil(random.random() * density)

        for elem in range(amount):
            color = self.colors[random.randint(0, self.num_colors)]
            particle = Particle.Particle(
                self.scenario, self.position[:], color, size=self.size, max_time2live=self.t2l)
            particle.speed = [random.randint(-part_speed, part_speed),
                              random.randint(-part_speed, part_speed)]
            self.scenario.particles.add(particle)


class CircularGenerator():
    """CircularGenerator

    Args:
        scenario (Scenario.Scenario): belonging to scenario.
        position (_type_): position of the generator.
        colors (list of tuples): possible colors of the particle as a list of 3 elements tuples.
        size (list, optional): dimension of the particles. Defaults to [1, 1].
        t2l (float, optional): Maximum lifespan of the particles. Defaults to 1.0.
    """

    def __init__(self, scenario: Scenario, position, colors, size=[1, 1], t2l=5.0) -> None:
        self.scenario = scenario
        self.position = position
        self.colors = colors
        self.size = size
        self.num_colors = len(colors)-1
        self.t2l = t2l

    def generate(self, part_speed=10.0, density=500.0, noise=3.0):
        """generate

        Particles are generated by the object evenly distributed on a circlular fashion at each generation.
        Origin of the particles is the position of the generator plus some random noise.

        Args:
            part_speed (float, optional): Actual speed of the particles. Defaults to 10.
            density (float, optional): Density factor of the generation. Number of generated particles is equal to the ceil rand[0, 1) * density.  Defaults to 200.0.
            noise (float, optional): Max noise of the starting position of each pasrticle on both axis. Defaults to 3.0.
        """

        amount = random.random() * density
        k_angle = 2*math.pi / amount

        for elem in range(math.ceil(amount)):
            color = self.colors[random.randint(0, self.num_colors)]
            position = [self.position[0]+random.randint(0, noise),
                        self.position[1]+random.randint(0, noise)]
            particle = Particle.Particle(
                self.scenario, position, color, size=self.size, max_time2live=self.t2l)
            particle.speed = [-math.cos(k_angle*elem) * part_speed,
                              math.sin(k_angle*elem) * part_speed]

            self.scenario.particles.add(particle)
