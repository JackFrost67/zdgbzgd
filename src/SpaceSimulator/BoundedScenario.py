"""Simulation Scenario with boundaries
It represents the simulation session and the pygame screen instance.
"""

import pygame
from SpaceSimulator import SimObject, Scenario

class BoundedScenario(Scenario.Scenario):
    """BoundedScenario, child of Scenario with bound detection mechanism.

        Args:
            screen (pygame.Surface, optional): pass the target display object, otherwise a new one will be created. Defaults to None.
            screen_size (tuple, optional): pygame.display screen size. This will be rescaled. Defaults to (1920, 1080).
            bgd_color (list, optional): pygame.display background RGB filling. Defaults to [0, 0, 0].
            debug (bool, optional): enable debug: more informations will be displayed at runtime. Defaults to False.
            testing (bool, optional): create headless display, only for testing purposes. Defaults to False.
    """

    def __init__(self,
                 screen: pygame.Surface,
                 screen_size,
                 bgd_color=[0, 0, 0],
                 debug=False,
                 testing=False) -> None:
        super().__init__(screen=screen, screen_size=screen_size,
                         bgd_color=bgd_color, debug=debug, testing=testing)
        self.boundaries = pygame.sprite.Group()

    def touch_y(self, obj):
        """touch_y, it inverts x speed component of obj.

        Args:
            obj (SimObject): the object which touches the y axis boundaries.
        """
        obj.speed = [-obj.speed[0], obj.speed[1]]

    def touch_x(self, obj):
        """touch_x, it inverts y speed component of obj.

        Args:
            obj (SimObject): the object which touches the x axis boundaries.
        """
        obj.speed = [obj.speed[0], -obj.speed[1]]

    def remove_outsiders(self):
        """remove_outsiders removes sprites belonging to self.all which are outside self.map_area"""

        for sprite in self.all:
            if sprite.position[0] < 0 or sprite.position[0] > self.map_area[0]:
                self.all.remove(sprite)
            if sprite.position[1] < 0 or sprite.position[1] > self.map_area[1]:
                self.all.remove(sprite)

    def show_boundaries(self):
        """show_boundaries draws the bounding box using a sprite"""
        boundaries = [[[self.map_area[0]/2, 0], [self.map_area[0], 3]],
                      [[self.map_area[0]/2, self.map_area[1]], [self.map_area[0], 3]],
                      [[0, self.map_area[1]/2], [3, self.map_area[1]]],
                      [[self.map_area[0], self.map_area[1]/2], [3, self.map_area[1]]]
                      ]

        for x, y in boundaries:
            image = pygame.Surface(y)
            image.fill([255, 255, 0])
            bound = SimObject.SimObject(self, x, image)
            bound.speed = [0, 0]
            self.boundaries.add(bound)
            self.all.add(bound)

    def main_logic(self):
        """main_logic with bound detection mechanism."""
        # this will be replaced via
        for sprite in self.all:
            if sprite.position[0] < 0 or sprite.position[0] > self.map_area[0]:
                self.touch_y(sprite)
            if sprite.position[1] < 0 or sprite.position[1] > self.map_area[1]:
                self.touch_x(sprite)

        super().main_logic()

    def main_loop(self):
        """main_loop for drawing the bounding box"""
        if self.debug:
            self.show_boundaries()
        super().main_loop()
