"""Simple simulation with Particle Generators and moving objects in a bounded scenario"""

from SpaceSimulator.BoundedScenario import BoundedScenario
from SpaceSimulator.Scenario import Scenario
from SpaceSimulator.SimObject import SimObject
from SpaceSimulator.ParticleGenerator import StandardGenerator

import random
import pygame
screen = pygame.display.set_mode((1366,768))
screen_size = (1366, 768)

class star(SimObject):

    def __init__(self, scenario: Scenario, position: list, image: pygame.Surface, debug=False, lock_on=False) -> None:
        super().__init__(scenario, position, image, debug, lock_on)
        self.gen = StandardGenerator(scenario, position, colors=[
                                     (255, 255, 255), (255, 0, 255)], size=[1, 1])

    def update(self, *args):
        super().update(*args)
        self.gen.generate(part_speed=30, density=1.5)


pygame.init()

# Setting up the screen
instance = BoundedScenario(screen = screen, debug=True, screen_size=(1000, 1000))

'''testing the stars sprites'''
for x in range(40):
    position = [random.randint(1, instance.map_area[0]),
                random.randint(1, instance.map_area[1])]
    image = pygame.Surface([10, 10], pygame.SRCALPHA, )
    image.fill([0, 0, 255])
    test = star(instance, position, image)

    test.speed = [random.uniform(-100, 100), random.uniform(-100, 100)]
    instance.all.add(test)
instance.main_loop()
