from SpaceSimulator import Scenario
from SpaceSimulator import UserControlled

import pygame
import os

os.environ["SDL_VIDEODRIVER"] = "dummy"
screen = pygame.display.set_mode((1366,768))
screen_size = (1366, 768)
# test initialization of UserControlled object


def test_userControlled_init():
    scenario = Scenario.Scenario(screen=screen, screen_size=screen_size,testing=True)
    obj = UserControlled.UserControlled(
        scenario, [0, 0], pygame.Surface([10, 10]))
    assert(obj._controlled == False)

# mock test of clicking a key on the keyboard to control the UserControlled object


def test_userControlled_controls():
    scenario = Scenario.Scenario(screen=screen, screen_size=screen_size,testing=True)
    obj = UserControlled.UserControlled(
        scenario, [0, 0], pygame.Surface([10, 10]))
    obj._controlled = True
    scenario.delta_time = 1

    pygame.K_UP = 0
    pygame.K_DOWN = 1
    pygame.K_LEFT = 2
    pygame.K_RIGHT = 3

    keys = [1, 0, 0, 0]
    obj.update(keys)
    assert(obj.speed[0] == obj.acceleration)

    obj.speed = [0.0, 0.0]
    keys = [0, 1, 0, 0]
    obj.update(keys)
    assert(obj.speed[0] == -obj.acceleration)

    obj.speed = [0.0, 0.0]
    keys = [0, 0, 1, 0]
    obj.update(keys)
    assert(obj.angle == obj.spin_factor)

    obj.speed = [0.0, 0.0]
    obj.angle = 0.0
    keys = [0, 0, 0, 1]
    obj.update(keys)
    assert(obj.angle == -obj.spin_factor)
