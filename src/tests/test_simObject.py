from SpaceSimulator import Scenario
from SpaceSimulator import SimObject

import pygame
import os

os.environ["SDL_VIDEODRIVER"] = "dummy"
screen = pygame.display.set_mode((1366,768))
screen_size = (1366, 768)
# Test the instantiation of a simObject


def test_simObject_instantiation():
    pygame.init()
    instance = Scenario.Scenario(screen=screen, screen_size=screen_size, testing=True)
    obj = SimObject.SimObject(instance, [0, 0], pygame.Surface([10, 10]))
    assert(obj.position == [0, 0])

# Test is_in_screen method


def test_simObject_is_in_screen():
    pygame.init()
    instance = Scenario.Scenario(screen=screen, screen_size=screen_size, testing=True)
    obj = SimObject.SimObject(instance, [0, 0], pygame.Surface([10, 10]))
    assert(obj.is_in_screen() == True)
    obj.position = [instance.map_area[0] + instance.camera_x + 1, 0]
    assert(obj.is_in_screen() == False)
    obj.position = [0, instance.map_area[1] + instance.camera_y + 1]
    assert(obj.is_in_screen() == False)
    obj.position = [instance.map_area[0] + instance.camera_x +
                    1, instance.map_area[1] + instance.camera_y + 1]
    assert(obj.is_in_screen() == False)

# Test that simObject is in a position different from the starting position giving a speed


def test_simObject_speed():
    pygame.init()
    instance = Scenario.Scenario(screen=screen, screen_size=screen_size,testing=True)
    instance.delta_time = 1  # set the delta time

    obj = SimObject.SimObject(instance, [0, 0], pygame.Surface([10, 10]))
    obj.speed = [1, 1]
    obj.update()
    assert(obj.position == [1, 1])


def test_lock_on_camera():
    pygame.init()
    instance = Scenario.Scenario(screen=screen, screen_size=screen_size,testing=True)
    instance.delta_time = 1  # set the delta time

    obj = SimObject.SimObject(
        instance, [0, 0], pygame.Surface([10, 10]), lock_on=True)
    obj.speed = [4, 4]
    obj.update()
    assert(obj.position == [instance.camera_x, instance.camera_y])
