
from SpaceSimulator.BoundedScenario import BoundedScenario
from SpaceSimulator.SimObject import SimObject
from SpaceSimulator.Scenario import Scenario
import pygame
import os

os.environ["SDL_VIDEODRIVER"] = "dummy"
screen = pygame.display.set_mode((1366,768))
screen_size = (1366, 768)

class Static(SimObject):
    def __init__(self, scenario: Scenario, position, debug=False, lock_on=False) -> None:
        image = pygame.Surface([10, 10], pygame.SRCALPHA, )
        image.fill([0, 255, 0])
        super().__init__(scenario, position, image, debug, lock_on)

    def __str__(self):
        return "I am not moving"


class square(SimObject):
    def __init__(self, scenario: Scenario, position, debug=False, lock_on=False) -> None:
        image = pygame.Surface([10, 10], pygame.SRCALPHA, )
        image.fill([0, 255, 0])
        super().__init__(scenario, position, image, debug, lock_on)


class CollisionScenario(BoundedScenario):
    def main_logic(self):
        """The main logic of this scenario"""
        # keys events

        pygame.sprite.groupcollide(self.objects, self.harmful, True, False)
        super().main_logic()


def test_collision():

    pygame.init()
    instance = CollisionScenario(screen = screen, screen_size=(1366,768), testing=True)
    instance.objects = pygame.sprite.Group()
    instance.harmful = pygame.sprite.Group()
    instance.map_area = [1000, 1000]

    for x in range(200):
        position = [900, 500+x]

        test = square(instance, position)
        test.speed = [100, 0]
        instance.objects.add(test)
        instance.all.add(test)

    image = pygame.Surface([20, 20], pygame.SRCALPHA)
    pygame.draw.polygon(image, color=(255, 255, 0), points=[
                        (10, 0), (0, 20), (20, 20)])

    test = Static(instance, [500, 500], debug=True)
    instance.harmful.add(test)
    instance.all.add(test)
    test = Static(instance, [700, 500], debug=True)
    instance.harmful.add(test)
    instance.all.add(test)

    for x in range(500):
        instance.delta_time = 0.16
        instance.all.update()
        instance.main_logic()

    assert(len(instance.all.sprites()) == 192)
