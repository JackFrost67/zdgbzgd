# from SpaceSimulator import Scenario
# from SpaceSimulator import SimObject
from SpaceSimulator import BoundedScenario, SimObject

import pygame
import random
import os

os.environ["SDL_VIDEODRIVER"] = "dummy"
screen = pygame.display.set_mode((1366,768))

# Test that Scenario class is instantiated correctly

def test_boundedScenario_instantiation():
    pygame.init()
    instance = BoundedScenario.BoundedScenario(screen= screen,
        bgd_color=[0, 255, 0], screen_size=(1366, 768), testing=True)
    assert(instance.screen_size == (1366, 768))
    assert(instance.bgd_color == [0, 255, 0])

# Test that all Sprites in the Scenario are added to the all group


def test_boundedScenario_add_sprite():
    pygame.init()
    scenario = BoundedScenario.BoundedScenario(screen = screen,
        bgd_color=[0, 255, 0], screen_size=(1366, 768), testing=True)
    # create a scenario
    # scenario.delta_time = 0.1 # set the delta time

    for _ in range(200):
        position = [random.randint(1, scenario.map_area[0]),
                    random.randint(1, scenario.map_area[1])]
        image = pygame.Surface([10, 10], pygame.SRCALPHA, ).convert()
        image.fill([0, 0, 255])
        obj = SimObject.SimObject(scenario, position, image)

        scenario.all.add(obj)
        # scenario.main_logic()
        # pygame.display.update()

    assert(len(scenario.all) == 200)

# Test that all Sprites in the Scenario are removed after purge


def test_boundedScenario_purge():
    pygame.init()
    scenario = BoundedScenario.BoundedScenario(screen = screen,
        bgd_color=[0, 255, 0], screen_size=(1366, 768), testing=True)  # create a scenario

    # add some sprites
    for _ in range(200):
        position = [random.randint(1, scenario.map_area[0]),
                    random.randint(1, scenario.map_area[1])]
        image = pygame.Surface([10, 10], pygame.SRCALPHA, ).convert()
        image.fill([0, 0, 255])
        obj = SimObject.SimObject(scenario, position, image)

        scenario.all.add(obj)

    scenario.purge()

    # checking they were removed
    assert(len(scenario.all) == 0)

# Test that all the Sprites hitting the bound are bounced correctly


def test_boundedScenario_inBoundaries():
    pygame.init()
    scenario = BoundedScenario.BoundedScenario(screen = screen,
        bgd_color=[0, 255, 0], screen_size=(1366, 768), testing=True)  # create a scenario
    scenario.map_area = [100, 100]

    # add some sprites
    for _ in range(50000):
        position = [random.randint(1, scenario.map_area[0]),
                    random.randint(1, scenario.map_area[1])]
        image = pygame.Surface([30, 30], pygame.SRCALPHA, ).convert()
        image.fill([0, 255, 255])
        obj = SimObject.SimObject(scenario, position, image)
        obj.speed = [random.uniform(-100, 100), random.uniform(-100, 100)]
        scenario.all.add(obj)

    scenario.remove_outsiders()
    scenario.main_logic()

    for sprite in scenario.all:
        assert((sprite.position[0] < 0 or sprite.position[0]
               > scenario.map_area[0]) == False)
        assert((sprite.position[1] < 0 or sprite.position[1]
               > scenario.map_area[1]) == False)

# test if objects outside scenario.map_area are correctly removed


def test_boundedScenario_outOfBoundaries():
    pygame.init()
    scenario = BoundedScenario.BoundedScenario(screen = screen,
        bgd_color=[0, 255, 0], screen_size=(1366, 768), testing=True)  # create a scenario
    scenario.map_area = [100, 100]

    for _ in range(100):
        position = [random.randint(scenario.map_area[0], scenario.map_area[0]*2),
                    random.randint(scenario.map_area[1], scenario.map_area[1]*2)]
        image = pygame.Surface([30, 30], pygame.SRCALPHA, ).convert()
        image.fill([0, 255, 255])
        obj = SimObject.SimObject(scenario, position, image)
        obj.speed = [random.uniform(-100, 100), random.uniform(-100, 100)]
        scenario.all.add(obj)

    position = [50, 50]
    image = pygame.Surface([30, 30], pygame.SRCALPHA, ).convert()
    image.fill([0, 255, 255])
    obj = SimObject.SimObject(scenario, position, image)
    obj.speed = [0, 0]
    scenario.all.add(obj)

    scenario.remove_outsiders()
    scenario.main_logic()

    assert(scenario.all.__len__() == 1)
