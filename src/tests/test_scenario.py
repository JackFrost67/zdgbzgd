from SpaceSimulator import Scenario
from SpaceSimulator import SimObject

import pygame
import random
import os

os.environ["SDL_VIDEODRIVER"] = "dummy"
screen = pygame.display.set_mode((1366,768))
screen_size = (1366, 768)

# Test that Scenario class is instantiated correctly


def test_scenario_instantiation1():
    pygame.init()
    scenario = Scenario.Scenario(screen=screen, screen_size=screen_size, testing=True)
    assert(scenario.screen_size == (1366, 768))


def test_scenario_instantiation2():
    pygame.init()
    scenario = Scenario.Scenario(screen_size=((1360,768)),
        bgd_color=[0, 255, 0], screen = pygame.display.set_mode((1360,768)), testing=True)
    assert(scenario.screen_size == (1360, 768))
    assert(scenario.bgd_color == [0, 255, 0])

# Test that all Sprites in the Scenario are added to the all group


def test_scenario_add_sprite():
    pygame.init()
    scenario = Scenario.Scenario(screen=screen, screen_size=screen_size, testing=True)  # create a scenario
    # scenario.delta_time = 0.1 # set the delta time

    for _ in range(200):
        position = [random.randint(1, scenario.map_area[0]),
                    random.randint(1, scenario.map_area[1])]
        image = pygame.Surface([10, 10], pygame.SRCALPHA, ).convert()
        image.fill([0, 0, 255])
        obj = SimObject.SimObject(scenario, position, image)

        scenario.all.add(obj)
        # scenario.main_logic()
        # pygame.display.update()

    assert(len(scenario.all) == 200)

# Test that all Sprites in the Scenario are removed after purge


def test_scenario_purge():
    pygame.init()
    scenario = Scenario.Scenario(screen=screen, screen_size=screen_size,testing=True)  # create a scenario

    # add some sprites
    for _ in range(200):
        position = [random.randint(1, scenario.map_area[0]),
                    random.randint(1, scenario.map_area[1])]
        image = pygame.Surface([10, 10], pygame.SRCALPHA, ).convert()
        image.fill([0, 0, 255])
        obj = SimObject.SimObject(scenario, position, image)

        scenario.all.add(obj)

    scenario.purge()

    # checking they were removed
    assert(len(scenario.all) == 0)
