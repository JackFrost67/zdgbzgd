import subprocess

program_list = ['demoAsteroids.py','demoCollisions.py','demoControllSpaceship.py','demoControl.py','demoHud.py','demoLockonBound.py','demoLockon.py','demoMenu.py','demoParticle.py','demoParticles.py','demoSimple.py']

for program in program_list:
    subprocess.call(['python', program])
    print("Finished:" + program)

