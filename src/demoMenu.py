import pygame

from Game.Menu import StartMenu


# ### FULLSCREEN CONTEST
# pygame.init()
# flags = pygame.SCALED | pygame.FULLSCREEN
# display_info = pygame.display.Info()
# width = display_info.current_w
# heigth = display_info.current_h
# fullscreen_contest = pygame.display.set_mode((width, heigth), flags, vsync=0)

## WINDOWED CONTEST
pygame.init()
width = 1000
heigth = 600
contest = pygame.display.set_mode((width, heigth), vsync=0)


# while (True):
# StartMenu.StartMenu(contest, "Pygame Start Menu")
StartMenu(contest, "Space Simulator")


