import pygame
from Game import GameScenario, UserControlledSpaceship
from SpaceSimulator import SimObject


class Hud(SimObject.SimObject):
    """Hud inehrits from SimObject.SimObject
    Defines a HUD for keep track of the player's life and ammo"""
    def __init__(self, scenario: GameScenario, user: UserControlledSpaceship):
        super().__init__(scenario, [scenario.screen_size[0], scenario.screen_size[1]], pygame.Surface(
            [scenario.screen_size[0] // 5, scenario.screen_size[1] // 5], pygame.SRCALPHA,), debug=False, lock_on=False)
        
        # RGB values for orange color (255, 165, 0)
        self._image.set_alpha(75)
        self._image.fill([255, 165, 0])
        self._icons = self.load_icon()
        self._icon_width = 50
        self._icon_height = 50
        self.debug_font = pygame.font.SysFont("monospace", 30)
        self.debug_font.set_bold(True)
        self.user = user

        # Tiles for the HUD
        self.tiles = self.create_tiles()

        # Adding hud to his own group
        scenario.hud.add(self)

    def load_icon(self):
        """ Load icons for the HUD """
        # read images
        heart = pygame.image.load("./assets/heart.png")
        ammo = pygame.image.load("./assets/ammo.png")

        images = {
            "heart": heart,
            "ammo": ammo
        }

        return images

    def create_tiles(self):
        """ Create tiles for the HUD """
        position = self.rect.topleft
        w = self._size[0] // 100
        h = self._size[1] // 100
        render_pos = [position[0] + w, position[1] + h]

        tiles = []

        for image_name, image in self._icons.items():
            image_scale = self.scale_image(
                image, w=self._icon_width, h=self._icon_height)
            rect = image_scale.get_rect(topleft=render_pos)

            tiles.append(
                {
                    "name": image_name,
                    "icon": image_scale,
                    "rect": rect
                }
            )

            render_pos[1] += image_scale.get_height() + 5

        return tiles

    def main_logic(self):
        """ main logic for the hud """
        # display text near the icons
        self._scenario.screen.blit(self.debug_font.render(
            str(self.user._life), True, (255, 255, 255)), (self.rect.topleft[0] + (self._icon_width * 2), self.rect.topleft[1] + (self._icon_width / 2) - 10))
        self._scenario.screen.blit(self.debug_font.render(
            str(self.user._ammo), True, (255, 255, 255)), (self.rect.topleft[0] + (self._icon_width * 2), self.rect.topleft[1] + (self._icon_width / 2) + 50))

    def update(self, *args):
        """ Update the hud """
        self.main_logic()
        for tile in self.tiles:
            self._scenario.screen.blit(tile["icon"], tile["rect"].topleft)

        self._scenario.screen.blit(self._image, self.rect.topleft)

    def scale_image(self, image, w=None, h=None):
        """ Scale image to the given size """
        if (w == None) and (h == None):
            pass
        elif h == None:
            scale = w / image.get_width()
            h = scale * image.get_height()
            image = pygame.transform.scale(image, (int(w), int(h)))
        elif w == None:
            scale = h / image.get_height()
            w = scale * image.get_width()
            image = pygame.transform.scale(image, (int(w), int(h)))
        else:
            image = pygame.transform.scale(image, (int(w), int(h)))

        return image
