import pygame
import json
import random

from Game import GameScenario, Hud, UserControlledSpaceship, Asteroid


class GameMain():
    """ GameMain is the main class of the game, it loads the settings, starts the display, the instances and starts the main loop

        Args:
            setting: the settings file to load, it is a json file formatted as default.json
        """

    def __init__(self, setting) -> None:
        pygame.quit()
        pygame.init()
        self.setting = setting + ".json"
        self.flags = pygame.SCALED | pygame.FULLSCREEN
        display_info = pygame.display.Info()
        self.screen_width = display_info.current_w
        self.screen_heigth = display_info.current_h
        self.loadSettings()
        screen = pygame.display.set_mode(
            (self.screen_width, self.screen_heigth), self.flags, vsync=0)
        self.instance = GameScenario.GameScenario(screen, screen_size=(
            self.screen_width, self.screen_heigth), debug=self.debug, testing=self.testing)
        self.loadInstances()
        self.instance.main_loop()

    def loadSettings(self):
        """loadSettings
            Loads the MAIN settings from the json file"""
        self.data = json.load(open(self.setting))
        self.debug = self.data["debug"]
        self.testing = self.data["testing"]
        self.map_width = self.data["map"]["width"]
        self.map_heigth = self.data["map"]["heigth"]

    def loadInstances(self):
        """loadInstances
            Loads the instances with settings from the json file"""
        # LOAD MAP
        """loading the map instance as defined in the json file"""
        self.instance.map_area = [self.map_width, self.map_heigth]
        print("map_Loaded")
        
        # LOAD SPACESHIP
        """loading the usercontrolled spaceship"""
        ss_data = self.data["spaceship"]
        user = UserControlledSpaceship.UserControlledSpaceship(self.instance, [300, 300], debug=ss_data["debug"], lock_on=True, controlled=True, acceleration=ss_data[
                                                               "acceleration"], life=ss_data["health"], max_speed=ss_data["maxSpeed"], ammo=ss_data["ammo"], fire_rate=ss_data["fireRate"])
        self.instance.all.add(user)
        print("spaceship_Loaded")

        # LOAD HUD
        """ loading HUD"""
        hud = Hud.Hud(self.instance, user)
        self.instance.all.add(hud)
        print("hud_Loaded")

        # LOAD Asteroids
        '''creating amount of asteroids as defined in the json file'''
        for x in range(self.data["asteroids"]["amount"]):
            position = [random.randint(1, self.instance.map_area[0]),
                        random.randint(1, self.instance.map_area[1])]
            test = Asteroid.Asteroid(self.instance, position)
            self.instance.all.add(test)
