import pygame
import math

from SpaceSimulator import UserControlled
from Game import Bullet, GameScenario

class UserControlledSpaceship(UserControlled.UserControlled):
    """UserControlledSpaceship child of UserControlled with additional attributes
    
        Args:
            scenario(GameScenario): The scenario the spaceship is in
            position(list): The starting position of the spaceship
            debug(bool, optional): Whether the spaceship is in debug mode. Defaults to False.
            lock_on(str,optional): Whether the spaceship is locked on. Defaults to True.
            controlled(bool,optional): Whether the spaceship is controlled. Defaults to True.
            acceleration(int,optional): The acceleration of the spaceship. Defaults to 1000.
            life(int,optional): The life of the spaceship. Defaults to 100.
            max_speed(int,optional): The maximum speed of the spaceship. Defaults to 1000.
            ammo(int,optional): The ammo of the spaceship. Defaults to 100.
            fire_rate(int,optional): The fire rate of the spaceship. Defaults to 5.
            """
    def __init__(self, scenario: GameScenario, position: list, debug=False, lock_on=True, controlled=True, acceleration=1000, life=100, max_speed=1000, ammo=100, fire_rate=5) -> None:
        image = pygame.image.load("./assets/ship.png")
        super().__init__(scenario, position, pygame.Surface(
            (image.get_size())), debug, lock_on, controlled)

        self.max_speed = max_speed
        self.acceleration = acceleration
        self._life = life
        self._image = image
        self.rect = self._image.get_rect(center=position)
        self._ammo = ammo
        self._fire_rate = fire_rate
        self._bullet_timer = 0

    def collision_detection(self, damage=1):
        """Detects collisions with other objects and applies damage to them"""
        # check for sprite collisions between spaceship, bullets, objects and boundaries
        for group in [self._scenario.objects, self._scenario.boundaries]:
            if pygame.sprite.spritecollideany(self, group) and self._life > 0:
                self._life -= damage

    def fire(self):
        """Fires a bullet"""
        self._bullet_timer = 1/self._fire_rate

        # fire a bullet if the fire rate allows it
        if self._ammo <= 0:
            return

        self._ammo -= 1
        # position respect the position and angle of the spaceship
        pos = [self.rect.centerx + 1 * math.cos(math.radians(self.angle)),
               self.rect.centery + 1 * -math.sin(math.radians(self.angle))]

        bullet = Bullet.Bullet(
            self._scenario, pos, self.angle)
        self._scenario.bullets.add(bullet)

    def controls(self, pressedKeys):
        """Controls the spaceship"""
        super().controls(pressedKeys)
        if pressedKeys[pygame.K_SPACE] and self._bullet_timer <= 0:
            self.fire()

    def update(self, pressedKeys):
        """Updates the spaceship life and bullets"""
        if self._life <= 0:
            self._scenario.all.remove(self)
            return

        if self._bullet_timer > 0:
            self._bullet_timer -= self._scenario.delta_time

        self.collision_detection()
        super().update(pressedKeys)

    def __str__(self):
        """Returns a string representation of the spaceship"""
        return super().__str__() + "\nBullets: " + str(len(self._scenario.bullets.sprites())) + "\nLife: " + str(self._life)
