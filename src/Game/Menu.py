import pygame
import sys

from Game.Game import GameMain

# hardcoded theme
font = "comfortaa"
yellow = (255, 255, 0)
blue = (0, 0, 255)
bgd_color = (0, 0, 0)
##


def drawMenuLayout(contest, title):
    contest.fill(bgd_color)
    if title != None:
        pygame.display.set_caption(title)


class Menu():
    """Menu class is used to create a menu."""
    def __init__(self, contest, title=None) -> None:
        self.contest = contest
        self.title = title
        drawMenuLayout(self.contest, self.title)
        pygame.display.update()

class CreditMenu(Menu):
    """CreditMenu class is used to create a credit menu."""
    def __init__(self, contest, title) -> None:
        self.contest = contest
        self.title = title
        super().__init__(contest, title)
        while True:
            self.draw()
            self.handle()
            pygame.display.update()

    def draw(self) -> None:
        """Draw defines what to draw into the menu."""
        title_font=pygame.font.SysFont(font,70)
        title_text=title_font.render('Credits',1,blue)
        self.contest.blit(title_text,(20,10))

        start = 120
        i = 0
        options = ['Created by:',
                    '   Michele Pugno',
                    '   Fabio D\'Elia',
                    '   Mattia Sgro',
                    '',
                    'press (enter) to go back to the main menu',
                    ]
        for opt in options:
            title = pygame.font.SysFont(font,30)
            text = title.render(opt, 1, yellow)
            self.contest.blit(text,(20, start+i*50))
            i += 1
        pass
    
    def handle(self) -> None:
        """Handle defines what to do when pressing defined buttons."""

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    sys.exit()
                if event.key == pygame.K_RETURN:
                    StartMenu(self.contest, self.title)



class InfoMenu(Menu):
    """InfoMenu class is used to create a info menu."""
    def __init__(self, contest, title) -> None:
        self.contest = contest
        self.title = title
        super().__init__(contest, title)
        while True:
            self.draw()
            self.handle()
            pygame.display.update()

    def draw(self) -> None:
        """Draw defines what to draw into the menu."""
        title_font = pygame.font.SysFont(font, 70)
        title_text = title_font.render('Instructions', 1, blue)
        self.contest.blit(title_text, (20, 10))

        start = 120
        i = 0
        options = ['Keys:',
                   '   arrow keys to move the (Spaceship)',
                   '   space to fire',
                   '   press (esc) to prompt the pause menu',
                   'Game:',
                   '   the goal of the game is to gain score by destroing the ',
                   '   asteroids without hitting them with the spaceship',
                   '   the game ends when the spaceship is destroyed',
                   'press (enter) to go back to the main menu',
                   ]
        for opt in options:
            title = pygame.font.SysFont(font, 30)
            text = title.render(opt, 1, yellow)
            self.contest.blit(text, (20, start+i*50))
            i += 1
        pass

    def handle(self) -> None:
        """Handle defines what to do when pressing defined buttons."""

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    sys.exit()
                if event.key == pygame.K_RETURN:
                    StartMenu(contest = self.contest, title = self.title)

class PauseMenu(Menu):
    """PauseMenu class is used to create a pause menu."""
    def __init__(self, contest, title, callback) -> None:
        self.contest = contest
        self.title = title
        self.run = True
        super().__init__(contest, title)
        while self.run:
            self.draw()
            self.handle(callback)
            pygame.display.update()
        pass

    def draw(self) -> None:
        """Draw defines what to draw into the menu."""
        title_font = pygame.font.SysFont(font, 70)
        title_text = title_font.render('Pause', 1, blue)
        self.contest.blit(title_text, (20, 10))

        start = 120
        i = 0
        options = ['Options:',
                   '   (esc) to resume the game',
                   '   (enter) to quit the game',
                   '   (q) to  quit to the main menu',
                   ]
        for opt in options:
            title = pygame.font.SysFont(font, 30)
            text = title.render(opt, 1, yellow)
            self.contest.blit(text, (20, start+i*50))
            i += 1
        pass

    def handle(self, callback) -> None:
        """Handle defines what to do when pressing defined buttons."""
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    pygame.quit()
                    sys.exit()
                if event.key == pygame.K_ESCAPE:
                    callback.unfreeze()
                    self.run = False
                if event.key == pygame.K_q:
                    # return to main menu / destroy previous game
                    StartMenu(title = "Spaceship Game")


class StartMenu(Menu):
    """StartMenu class is used to create a start menu."""
    def __init__(self, contest=None, title=None) -> None:
        if contest == None:
            pygame.init()
            self.contest = pygame.display.set_mode((1000, 600))
        else:
            self.contest = contest
        self.title = title
        super().__init__(self.contest, self.title)
        while True:
            self.draw()
            self.handle()
            pygame.display.update()

    def draw(self) -> None:
        """Draw defines what to draw into the menu."""
        title_font = pygame.font.SysFont(font, 70)
        title_text = title_font.render('Main Menu', 1, blue)
        self.contest.blit(title_text, (20, 10))

        start = 120
        i = 0
        options = ['Press (enter) to start the game with the default settings',
                   'Press (c) to start the game with custom settings',
                   'Press (i) to see the instructions',
                   'Press (p) to see the credits',
                   'Press (esc) to quit',
                   '']

        for opt in options:
            title = pygame.font.SysFont(font, 30)
            text = title.render(opt, 1, yellow)
            self.contest.blit(text, (20, start+i*50))
            i += 1

    def handle(self) -> None:
        """Handle defines what to do when pressing defined buttons."""
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    sys.exit()
                if event.key == pygame.K_RETURN:
                    GameMain(setting="default")
                if event.key == pygame.K_c:
                    GameMain(setting="custom")
                if event.key == pygame.K_i:
                    InfoMenu(self.contest, self.title)
                if event.key == pygame.K_p:
                    CreditMenu(self.contest, self.title)
                pass
