import json
from SpaceSimulator import SimObject
from Game import GameScenario

import pygame
import math


class Bullet(SimObject.SimObject):
    """Bullet class, inherits from SimObject
    Staticly defines a bullet shot by the spaceship"""
    def __init__(self, scenario: GameScenario, position: list, angle: float, debug=False, lock_on=False) -> None:
        super().__init__(scenario, position, pygame.Surface((10, 10)), debug, lock_on)
        self.angle = angle
        vel = json.load(open('default.json'))["bullet"]["speed"]
        self.velocity = [vel, vel]
        self.speed = [math.cos(math.radians(self.angle)) * self.velocity[0],
                      -math.sin(math.radians(self.angle)) * self.velocity[1]]
        self.acceleration = 100
        self._image.fill((255, 0, 0))


