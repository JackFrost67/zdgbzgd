from SpaceSimulator import SimObject
from Game import GameScenario

import pygame
import random
import os


class Asteroid(SimObject.SimObject):
    def __init__(self, scenario: GameScenario, position: list, debug=False, lock_on=False) -> None:
        # randomly select an asteroid image from assets folder and create a surface from it with the same size
        filename = ["./assets/Asteroid_Big.png",
                    "./assets/Asteroid_Medium.png", "./assets/Asteroid_Small.png"]
        n = random.randint(0, 2)
        image = pygame.image.load(filename[n])
        image.set_colorkey((0, 0, 0))
        super().__init__(scenario, position, image, debug, lock_on)
        if n == 0:
            self.speed = [random.uniform(-25, 25), random.uniform(-25, 25)]
        elif n == 1:
            self.speed = [random.uniform(-50, 50), random.uniform(-50, 50)]
        elif n == 2:
            self.speed = [
                random.uniform(-100, 100), random.uniform(-100, 100)]

        self._scenario.objects.add(self)
        self._scenario.all.add(self)

    def __str__(self):
        return """Asteroid"""

    def update(self, *args):
        self.collision_detection()
        super().update()

    def collision_detection(self):
        # check for sprite collisions between spaceship, bullets
        if pygame.sprite.spritecollide(self, self._scenario.bullets, True):
            self._scenario.objects.remove(self)
            self._scenario.all.remove(self)
