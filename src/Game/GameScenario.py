# from Game.Menu import PauseMenu
from SpaceSimulator import BoundedScenario
from Game import Menu
import pygame
import sys


class GameScenario(BoundedScenario.BoundedScenario):
    """GameScenario inerits from BoundedScenario
    It defines the game scenario and its logic, its main loop and the menus it uses"""
    def __init__(self,
                 screen: pygame.Surface = None,
                 screen_size=(1920, 1080),
                 bgd_color=[0, 0, 0],
                 debug=False,
                 testing=False) -> None:

        super().__init__(screen, screen_size, bgd_color, debug, testing)
        self.bullets = pygame.sprite.Group()
        self.objects = pygame.sprite.Group()
        self.hud = pygame.sprite.Group()
        self.done = False

    def remove_outsiders(self):
        """remove_outsiders removes sprites belonging to self.all which are outside self.map_area"""
        for group in [self.bullets, self.objects]:
            for sprite in group:
                if sprite.position[0] < 0 or sprite.position[0] > self.map_area[0]:
                    group.remove(sprite)
                if sprite.position[1] < 0 or sprite.position[1] > self.map_area[1]:
                    group.remove(sprite)
        super().remove_outsiders()

    def main_logic(self):
        """Main game scenario logic"""
        self.remove_outsiders()
        self.bullets.update()
        self.objects.update()
        self.hud.update()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.freeze()
                    Menu.PauseMenu(self.screen, None, self)
        super().main_logic()            

    def remove_outsiders(self):
        """remove_outsiders removes sprites belonging to self.all which are outside self.map_area"""
        for sprite in self.bullets:
            if sprite.position[0] < 0 or sprite.position[0] > self.map_area[0]:
                self.bullets.remove(sprite)
            if sprite.position[1] < 0 or sprite.position[1] > self.map_area[1]:
                self.bullets.remove(sprite)
        # super().remove_outsiders()

    def main_loop(self):
        """Main game scenario loop"""
        super().main_loop()
